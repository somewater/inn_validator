require 'test_helper'

class InnTest < ActiveSupport::TestCase
  def assert_errors(inn, original)
    assert_not inn.valid?
    assert_equal original, inn.errors.details[:number].map{|h| h.to_a.first.last }
  end

  test "INN not provided" do
    assert_errors Inn.new, [:inn_is_nil]
  end

  test "INN with wrong length" do
    assert_errors Inn.new(number: '123'), [:inn_wrong_length]
  end

  test "INN contains unsupported symbols" do
    assert_errors Inn.new(number: 'aabbccddeeff'), [:inn_wrong_chars]
  end

  test "INN includes reserved century code" do
    assert_errors Inn.new(number: '111111011114'), [:inn_recerved_century]
    assert_errors Inn.new(number: '111111711119'), [:inn_recerved_century]
  end

  test "INN contains too young birthday" do
    # birth day is 2099-11-11
    assert_errors Inn.new(number: '991111611114'), [:inn_too_young]
  end

  test "INN contains too old birthday" do
    # birth day is 1812-11-11
    assert_errors Inn.new(number: '121111211119'), [:inn_too_old]
  end

  test "INN birthday includes wrong month" do
    # birth day is 2000-80-11
    assert_errors Inn.new(number: '008011611115'), [:inn_wrong_month]

    # birth day is 2000-00-11
    assert_errors Inn.new(number: '000011611113'), [:inn_wrong_month]
  end

  test "INN birthday includes wrong day" do
    # birth day is 2000-11-00
    assert_errors Inn.new(number: '001100611111'), [:inn_wrong_day]

    # birth day is 2000-11-32
    assert_errors Inn.new(number: '001132611114'), [:inn_wrong_day]
  end

  test "INN birthday includes nonexistent day" do
    # birth day is 2000-02-31
    assert_errors Inn.new(number: '000231611119'), [:inn_nonexistent_date]
  end

  test "check checksum" do
    # (1 * 4 + 1 * 6 + 3 * 7) mod 11  ->  31 mod 11  ->  9
    assert Inn.new(number:        '000101300009').valid?
    assert_errors Inn.new(number: '000101300001'), [:inn_checksum]
  end

  test "check checksum, step 2" do
    # (1 * 1 + 1 * 4 + 1 * 6 + 3 * 7) mod 11  ->  32 mod 11  ->  10; step 2 for checksum calculation
    # (1 * 3 + 1 * 6 + 1 * 8 + 3 * 9) mod 11  ->  44 mod 11  ->  0
    assert Inn.new(number:        '100101300000').valid?
  end
end
