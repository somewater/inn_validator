Rails.application.routes.draw do
  resources :inns, path: '/', only: [:new, :create], path_names: { new: '' }
end
