# Quickstart:

1. Download project.

1. Run server:

    ```
    rails s
    ```

1. Run tests:

    ```
    rails t
    ```