require 'inn_format_validator'

class Inn
  include ActiveModel::Model

  attr_accessor :number

  validates :number, inn_format: true
end