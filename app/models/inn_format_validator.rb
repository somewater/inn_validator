class InnFormatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.nil?
      record.errors.add attribute, :inn_is_nil
    elsif value.empty? || value.size != 12
      record.errors.add attribute, :inn_wrong_length
    elsif value =~ /\A[0-9]+\z/
      digits = value.split('').map(&:to_i)
      check_birthday record, attribute, digits
      check_checksum record, attribute, digits
    else
      record.errors.add attribute, :inn_wrong_chars
    end
  end

  def check_birthday(record, attribute, digits)
    if digits[6] == 0 or digits[6] >= 7
      record.errors.add attribute, :inn_recerved_century
    else
      now = Time.new
      year = digits[0] * 10 + digits[1]
      month = digits[2] * 10 + digits[3]
      day = digits[4] * 10 + digits[5]
      if month < 1 || month > 12
        record.errors.add attribute, :inn_wrong_month
      elsif day < 1 || day > 31
        record.errors.add attribute, :inn_wrong_day
      else
        century = (digits[6] - 1) / 2 + 19
        full_year = (century - 1) * 100 + year
        birthday = Time.new(full_year, month, day)
        if birthday.day != day
          # Ruby automatically convert nonexistent days to the nearest existing one
          # Example: Time.new(2000, 2, 31) -> 2000-03-02
          # But we can check such case comparing source and resulting day
          record.errors.add attribute, :inn_nonexistent_date
        elsif birthday > now
          record.errors.add attribute, :inn_too_young
        elsif now - birthday > 200.years
          record.errors.add attribute, :inn_too_old
        end
      end
    end
  end

  def check_checksum(record, attribute, digits)
    a12 = digits.take(11).each_with_index.map { |v, i| v * (i + 1) }.sum % 11
    if a12 == 10
      a12 = digits.take(11).each_with_index.map { |v, i| v * (((i + 2) % 11) + 1) }.sum % 11
    end
    if a12 == 10
      record.errors.add attribute, :inn_unused_number
    else
      record.errors.add attribute, :inn_checksum unless digits[11] == a12
    end
  end
end
