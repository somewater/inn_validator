class InnsController < ApplicationController
  def new
    @inn = Inn.new
  end

  def create
    p params
    @inn = Inn.new(inn_params)

    if @inn.valid?
      flash.now[:notice] = 'CORRECT'
    else
      flash.now[:alert] = 'BAD FORMAT'
    end

    render :new
  end

  private

  def inn_params
    params.require(:inn).permit(:number)
  end
end